package stream.camera.management.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stream.camera.management.dto.DeviceInfoDto;
import stream.camera.management.dto.DeviceInfoPlainDto;
import stream.camera.management.model.DeviceInfo;
import stream.camera.management.repository.DeviceInfoRepository;
import stream.camera.management.util.UtilMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeviceInfoService {

    private final Logger log = LoggerFactory.getLogger(DeviceInfoService.class);
    private DeviceInfoRepository deviceInfoRepository;
    @Autowired
    private UtilMapper utilMapper;

    @Autowired
    public DeviceInfoService(DeviceInfoRepository deviceInfoRepository) {
        this.deviceInfoRepository = deviceInfoRepository;
    }

    public void createDeviceInfo(List<DeviceInfoDto> deviceInfoDtoList) {
        for (DeviceInfoDto deviceInfoDto : deviceInfoDtoList) {
            DeviceInfo deviceInfo = new DeviceInfo(deviceInfoDto.getUuid(), deviceInfoDto.getSerialNumber(),
                    deviceInfoDto.getModel(), deviceInfoDto.getFirmwareVersion(), deviceInfoDto.getBuildDate(),
                    deviceInfoDto.getHardwareId(), deviceInfoDto.getActive(), deviceInfoDto.getIpAddressList(),
                    deviceInfoDto.getStreamUrlList(), deviceInfoDto.getCodec(), LocalDate.now(), deviceInfoDto.getCreateBy(),
                    null, null);
            this.deviceInfoRepository.save(deviceInfo);
            log.debug("Successfully save device info row into database");
        }
    }

    public List<String> getAllDeviceIds() {
        return getAllDeviceInfo().stream()
                .map(DeviceInfoPlainDto::getUuid)
                .collect(Collectors.toList());
    }

    public List<DeviceInfoPlainDto> getAllDeviceInfo() {
        List<DeviceInfo> deviceInfoList = deviceInfoRepository.findAll();
        log.debug("Successfully get all device info rows from database");
        List<DeviceInfoPlainDto> deviceInfoPlainDtoList = new ArrayList<>();
        deviceInfoList.forEach((deviceInfo) -> {
            DeviceInfoDto deviceInfoDto = new DeviceInfoDto();
            utilMapper.mapDeviceInfoToDeviceInfoDto(deviceInfo, deviceInfoDto);
            deviceInfoPlainDtoList.add(utilMapper.mapDeviceInfoDtoToDeviceInfoPlainDto(deviceInfoDto));
        });
        return deviceInfoPlainDtoList;
    }

    public DeviceInfoPlainDto getDeviceInfoByUuid(String uuid) {
        DeviceInfo deviceInfo = deviceInfoRepository.findByUuid(uuid);
        if (deviceInfo == null) {
            return null;
        }
        log.info("Successfully get device info row by uuid into database");
        DeviceInfoDto deviceInfoDto = new DeviceInfoDto();
        utilMapper.mapDeviceInfoToDeviceInfoDto(deviceInfo, deviceInfoDto);
        return utilMapper.mapDeviceInfoDtoToDeviceInfoPlainDto(deviceInfoDto);
    }

    public String[] getDeviceStreamUrlByUuid(String uuid) {
        DeviceInfo deviceInfo = deviceInfoRepository.findByUuid(uuid);
        log.debug("Successfully get device info row by uuid into database");

        return deviceInfo == null ? null : deviceInfo.getStreamUrlList();
    }

    public String[] getDeviceIpAddressByUuid(String uuid) {
        DeviceInfo deviceInfo = deviceInfoRepository.findByUuid(uuid);
        log.debug("Successfully get device info row by uuid into database");
        return deviceInfo == null ? null : deviceInfo.getIpAddressList();
    }

    public void updateDeviceActive(String uuid, boolean active) {
        DeviceInfo deviceInfo = deviceInfoRepository.findByUuid(uuid);
        if (deviceInfo != null) {
            deviceInfo.setActive(active);
            this.deviceInfoRepository.save(deviceInfo);
            log.debug("Successfully updated device uuid {" + uuid + "}");
        }
    }

    public void updateDeviceInfo(List<DeviceInfoDto> deviceInfoDtoList) {
        for (DeviceInfoDto deviceInfoDto : deviceInfoDtoList) {
            DeviceInfo originDeviceInfo = deviceInfoRepository.findByUuid(deviceInfoDto.getUuid());
            log.debug("Successfully get device info row by uuid into database");

            String serialNumber = deviceInfoDto.getSerialNumber() == null ? originDeviceInfo.getSerialNumber() : deviceInfoDto.getSerialNumber();
            String model = deviceInfoDto.getModel() == null ? originDeviceInfo.getModel() : deviceInfoDto.getModel();
            String firmwareVersion = deviceInfoDto.getFirmwareVersion() == null ? originDeviceInfo.getFirmwareVersion() : deviceInfoDto.getFirmwareVersion();
            LocalDate buildDate = deviceInfoDto.getBuildDate() == null ? originDeviceInfo.getBuildDate() : deviceInfoDto.getBuildDate();
            String hardwareId = deviceInfoDto.getHardwareId() == null ? originDeviceInfo.getHardwareId() : deviceInfoDto.getHardwareId();
            boolean active = deviceInfoDto.getActive() == null ? originDeviceInfo.getActive() : deviceInfoDto.getActive();
            String[] ipAddress = deviceInfoDto.getIpAddressList() == null ? originDeviceInfo.getIpAddressList() : deviceInfoDto.getIpAddressList();
            String[] streamUrlList = deviceInfoDto.getStreamUrlList() == null ? originDeviceInfo.getStreamUrlList() : deviceInfoDto.getStreamUrlList();
            String codec = deviceInfoDto.getCodec() == null ? originDeviceInfo.getCodec() : deviceInfoDto.getCodec();
            LocalDate createAt = deviceInfoDto.getCreateAt() == null ? originDeviceInfo.getCreateAt() : deviceInfoDto.getCreateAt();
            String createBy = deviceInfoDto.getCreateBy() == null ? originDeviceInfo.getCreateBy() : deviceInfoDto.getCreateBy();
            String updateBy = deviceInfoDto.getUpdateBy() == null ? originDeviceInfo.getUpdateBy() : deviceInfoDto.getUpdateBy();

            DeviceInfo deviceInfo = new DeviceInfo(deviceInfoDto.getUuid(), serialNumber, model, firmwareVersion,
                    buildDate, hardwareId, active, ipAddress, streamUrlList, codec, createAt, createBy, LocalDate.now(), updateBy);
            this.deviceInfoRepository.save(deviceInfo);
            log.info("Successfully save device info row into database");
        }
    }

    public void deleteDeviceInfo(List<String> uuidList) {
        for (String uuid : uuidList) {
            DeviceInfo deviceInfo = deviceInfoRepository.findByUuid(uuid);
            if (deviceInfo == null) {
                continue;
            }
            log.debug("Successfully get device info row by uuid into database");
            DeviceInfoDto deviceInfoDto = new DeviceInfoDto();
            utilMapper.mapDeviceInfoToDeviceInfoDto(deviceInfo, deviceInfoDto);
            deviceInfoRepository.delete(deviceInfo);
            log.debug("Successfully delete device info row into database");
        }
    }

    public void deleteAllDeviceInfo() {
        deviceInfoRepository.deleteAll();
        log.debug("Successfully delete all device info rows into database");
    }
}
